import React, { Component } from 'react';
import Nav from './Nav';
import NoteList from './NoteList';
import NoteForm from './NoteForm';

class App extends Component {

  // thay vì sử dụng set để thêm dữ liệu ta có thể dùng push
  // set thì tự đặt id, còn push thì mặc định được firebase random
  // pushData = () => {
  //   var data = firebase.database().ref("dataNote");
  //   data.push({
  //     title: "Ghi chú ngày 15/06/2019",
  //     noteContent: "Đi du lịch Tây Bắc"
  //   });
  //   alert("vừa thêm dữ liệu mới, vào firebase để kiểm tra");
  // }


  // //hiển thị dữ liệu
  // showData = () => {
  //   //lấy dữ liệu từ dataNode
  //   var data = firebase.database().ref("dataNote");
  //   //lấy giá trị được lưu lại bởi snapshot
  //   data.once('value').then(function (snapshot) {
  //     //in dữ liệu ra
  //     console.log(snapshot.val());
  //   });
  //   alert("vừa lấy dữ liệu ra, f12 để kiểm tra");
  // }

  // //xóa dữ liệu
  // removeData = (id)=>{
  //   //lấy dữ liệu từ dataNode
  //   var data = firebase.database().ref("dataNote");
  //   data.child(id).remove();
  //   alert("bạn vừa xóa dữ liệu với id: "+ id);
  // }


  render() {
    return (
      <div>
        {/* <button onClick={() => this.showData()}>Show</button>
        <button onClick={() => this.pushData()}>Push</button>
        <button onClick={() => this.removeData("--LhZuSsHz9NicSkvV_e8")}>Remove</button> */}
        
        <Nav/>
        <div className="content pt-3">
          <div className="container">
            <div className="row">
              <NoteList/>
              <NoteForm/>
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default App;
