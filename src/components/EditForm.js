import React, { Component } from 'react';
import { connect } from 'react-redux';

class EditForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: "",
            noteContent: "",
            id:""
        };
    }

    //lấy giá trị khi input thay đổi
    isChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({
            [name]: value
        });
    }

    //khi click nút sửa thì lưu lại thông tin
    componentWillMount() {
        if(this.props.editItem){
            this.setState({
                title: this.props.editItem.title,
                noteContent: this.props.editItem.noteContent,
                id:this.props.editItem.id
            })
        }
    }


    // Hàm sửa data
    //lưu những thay đổi ở ô input vào editObject
    //Sau đó truyền vào store action để thực hiện
    editData = () =>{
        var editObject = {};
        editObject.id = this.state.id;
        editObject.title = this.state.title;
        editObject.noteContent = this.state.noteContent;

        this.props.editDataStore(editObject);
        this.props.changeStatus();
    }

    render() {
        return (
            <div className="mt-2">
                <form className="p-3" style={{ border: '1px solid #000', borderRadius: '4px', padding: '5px' }}>
                    <h3 className="text-center" style={{ border: '1px solid darkgray', padding: '5px' }}>Sửa ghi chú</h3>
                    <div className="form-group">
                        <label htmlFor="title">Tilte:</label>
                        <input type="text" defaultValue={this.props.editItem.title} onChange={(event) => this.isChange(event)} className="form-control" name="title" id="title" placeholder="Nhập title" />
                    </div>
                    <div className="form-group">
                        <div className="form-group">
                            <label htmlFor="noteContent">Nội dung ghi chú:</label>
                            <textarea className="form-control" name="noteContent" id="noteContent" onChange={(event) => this.isChange(event)} placeholder="Nội dung ghi chú" rows={3} defaultValue={this.props.editItem.noteContent} />
                        </div>
                    </div>
                    <input type="button" onClick={()=>this.editData(this.state.title, this.state.noteContent)} value="Sửa" className="btn text-white btn-warning btn-block" /> &nbsp;
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        editItem : state.edit.editItem
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        editDataStore: (item) =>{
            dispatch({type:"POST_EDIT_DATA",item});
        },
        changeStatus: () => {
            dispatch({type: "CHANGE_STATUS"})
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditForm);