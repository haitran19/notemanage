import React, { Component } from 'react';
import { connect } from 'react-redux';
import EditForm from './EditForm';

class NoteForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: "",
            noteContent: "",
            id:""
        };
    }

    //lấy giá trị khi input thay đổi
    isChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({
            [name]: value
        });
    }

    

    //hàm addData cho nút Lưu
    addData = (title, noteContent) => {
        //khởi tạo một đối tượng item để lưu dữ liệu
        var item = {};
        item.title = title;
        item.noteContent = noteContent;
        //truyền item vào hàm addDataStore
        this.props.addDataStore(item);
    }

    showEditForm = ()=>{
        if(this.props.statusEdit.status===true){
            return <EditForm/>
        }
    }

    render() {
        return (
            <div className="col-4">
            <div>
                <form action="" style={{ border: '1px solid #000', borderRadius: '4px', padding: '5px' }}>
                    <h3 className="text-center" style={{ border: '1px solid darkgray', padding: '5px' }}>Thêm ghi chú</h3>
                    <div className="form-group">
                        <label htmlFor="title">Tiêu đề:</label>
                        <input type="text" onChange={(event) => this.isChange(event)} name="title" id="title" className="form-control" placeholder="Tên tiêu đề" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="noteContent">Nội dung ghi chú:</label>
                        <textarea className="form-control" onChange={(event) => this.isChange(event)} name="noteContent" id="noteContent" placeholder="Nội dung ghi chú" rows={3} defaultValue={""} />
                    </div>
                    <button type="reset" onClick={() => this.addData(this.state.title, this.state.noteContent)} className="btn btn-primary btn-block">Thêm</button>
                </form>
            </div>
                {
                    this.showEditForm()
                }
            </div>
        );
    }
}

//biến state thành props để có thể truyền sang store
const mapStateToProps = (state, ownProps) => {
    return {
        statusEdit:state.edit,
        editItem : state.edit.editItem
    }
}
//biến dispatch thành props để có thể thực thi
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        //khai báo hàm addDataaStore với tham số là iteme
        addDataStore: (item) => {
            //thực thi action ADD_DATA, action này của addReducer, và reducer lại đc thằng store quản lý
            //mà NoteForm lại kết nối với store nên nó sẽ có quyền gọi action của reducer
            dispatch({ type: "ADD_DATA", item })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NoteForm);