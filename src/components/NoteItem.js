import React, { Component } from 'react';
import { connect } from 'react-redux';



class NoteItem extends Component {

    //đổi trạng thái để hiển thị form sửa + lấy dữ liệu cần sửa push ra form
    editData = ()=>{
        this.props.changeStatus();
        this.props.getEditData(this.props.note);
    }

    //hàm xóa đc gọi khi click nút xóa. và truyền id vào hàm getDeleteData
    deleteData = ()=>{
        this.props.getDeleteData(this.props.note.id);
    }

    render() {
        return (
            <div className="card">
                <div className="card-header" style={{display: 'flex',flexWrap:'wrap'}} role="tab" id="note1">
                    <h5 className="mb-0">
                        <a data-toggle="collapse" data-parent="#noteList" href={"#num"+ this.props.i} aria-expanded="true" aria-controls="noteContent1">
                            {this.props.title}
                        </a>
                    </h5>
                    <div style={{position: 'absolute',right:'5px',top:'5px'}}>
                    <button type="button" onClick={()=>this.editData()} className="btn btn-outline-warning">Sửa</button><button type="button" onClick={()=>this.deleteData()} className="btn btn-outline-danger">Xóa</button>
                    </div>
                </div>
                <div id={"num"+this.props.i} className="collapse in" role="tabpanel" aria-labelledby="note1">
                    <div className="card-body">
                        {this.props.noteContent}
                    </div>
                </div>
            </div>
        );
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        changeStatus: () => {
            dispatch({type: "CHANGE_STATUS"})
        },
        getEditData: (editObject) => {
            dispatch({type: "GET_EDIT_DATA",editObject})
        },
        getDeleteData: (id) => {
            dispatch({type: "DELETE_DATA",id})
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NoteItem);