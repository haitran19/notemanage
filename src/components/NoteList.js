import React, { Component } from 'react';
import NoteItem from './NoteItem';
import { data } from '../connectData/firebaseConnect';

class NoteList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dataFirebase:[]
        }
    }
    

    //chay chương trình sẽ chạy willMount đầu tiên
    componentWillMount() {
        //hàm này để duyệt phần tử từ dữ liệu firebase
        data.on('value',(notes)=>{
            //tạo mảng để lưu dữ liệu khi duyệt. 
            //vì firebase trả về đối tượng nên ta phải cho về mảng để thao tác
            var arrayData = [];
            //duyệt bằng notes
            notes.forEach(element=>{
                //mỗi element là 1 notes
                const key = element.key;
                const title = element.val().title;
                const noteContent = element.val().noteContent;
                //thêm vào mảng
                arrayData.push({
                    id:key,
                    noteContent:noteContent,
                    title:title
                })
            });
            //truyền arrayData vào state dataFirebase để sử dụng
            this.setState({
                dataFirebase:arrayData
            });
        })
    }
    

    getData = ()=>{
        if(this.state.dataFirebase){
            //nếu có dữ liệu thì thực hiện vòng lặp map in ra NoteItem
            return this.state.dataFirebase.map((value,key)=>{
                return(
                    //truyền dạng props cho NoteItem
                    <NoteItem key={key} i={key} note={value} title={value.title} noteContent={value.noteContent}/>
                )
            })
        }
    }

    render() {
        return (
                <div className="col">
                    <h3 className="text-center">Danh sách ghi chú</h3>
                    <div id="noteList" role="tablist" aria-multiselectable="true">
                        {this.getData()} 
                    </div>
                    </div>
        );
    }
}

export default NoteList;