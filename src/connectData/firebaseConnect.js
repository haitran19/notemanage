//import firebase
import * as firebase from 'firebase';

// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyAzbS2WZcm3RGiM07xfpRzEuvj8H86TZYQ",
    authDomain: "notereact-d3a43.firebaseapp.com",
    databaseURL: "https://notereact-d3a43.firebaseio.com",
    projectId: "notereact-d3a43",
    storageBucket: "notereact-d3a43.appspot.com",
    messagingSenderId: "148962500164",
    appId: "1:148962500164:web:1ad00668c49863ad"
  };


  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
// kết nối vào bảng dataNote và export ra để có thể sử dụng
export const data = firebase.database().ref('dataNote');



// dưới đây là data set, đặt lại dữ liệu cho note1

// var data1 = firebase.database().ref("dataNote/note1");

// data1.set({
//     id:1,
//     title: "Ghi chú ngày 10/06/2019",
//     noteContent: "Đi du lịch Đà Nẵng"
// });