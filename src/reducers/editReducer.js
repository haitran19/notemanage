import { data } from '../connectData/firebaseConnect';

const editInitialState = {
    status: false,
    editItem:{}
}
const editReducer = (state = editInitialState, action) => {
    switch (action.type) {
        case "GET_EDIT_DATA":
            return {...state,editItem:action.editObject}

        case "POST_EDIT_DATA":
            data.child(action.item.id).update({
                title:action.item.title,
                noteContent:action.item.noteContent
            })
            alert("Đã cập nhật dữ liệu thành công!");
            return {...state,editItem:{}}

        case "DELETE_DATA":
            data.child(action.id).remove();
            alert("Đã xóa dữ liệu thành công!");
            return state

        case "CHANGE_STATUS":
            return {...state,status:!state.status}

        default:
            return state
    }
}






export default editReducer;