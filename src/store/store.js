import addReducer from '../reducers/addReducer';
import editReducer from '../reducers/editReducer';



//khai báo biến redux để chứa thư viện redux
var redux = require('redux');

// allReducer chứa tất cả các reducer con
const allReducer = redux.combineReducers({
    add:addReducer,
    edit:editReducer
});

//tạo store từ thư viện redux bằng lệnh createStore
//vì store quản lý reducer nên ta truyền vào allReducer
var store = redux.createStore(allReducer);

store.subscribe(function(){
    console.log(JSON.stringify(store.getState()))
})



export default store;